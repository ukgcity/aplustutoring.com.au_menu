jQuery(document).ready(function($) {

   var art = $("#rt-main .rt-containerInner")[0].getBoundingClientRect(); /* coords of rt-main */
   var zoom_index = (window.screen.availWidth/$("#rt-main .rt-container")[0].getBoundingClientRect().right).toFixed(2);
   var marker_width = ($("#rt-main .rt-container")[0].getBoundingClientRect().right - $("#rt-main .rt-container")[0].getBoundingClientRect().left).toFixed(2);

   if(localStorage.getItem('menu_source') && art.top < 300 && zoom_index == localStorage.getItem('menu_zoom_index')) {
     var menu_object1 = JSON.parse(localStorage.getItem('menu_source'));
   } else if(localStorage.getItem('menu_source') && art.top < 300 && marker_width == localStorage.getItem('marker_width') && Math.abs(zoom_index - localStorage.getItem('menu_zoom_index')) < 0.02) {
     var menu_object1 = JSON.parse(localStorage.getItem('menu_source'));
   } else if(localStorage.getItem('menu_source') && art.top > 300) {
     var menu_object1 = JSON.parse(localStorage.getItem('menu_source'));
   }

   if(menu_object1!=null) {
     var active_item_class = new Array();

     $("#rt-top .rt-fusionmenu .nopill .menutop .active").each(function(){
       var active_item_arr = new Array();

       active_item_arr = $(this).attr("class").split(" ");
       $.each(active_item_arr, function(index, value){
         if(value.indexOf("item") != -1) {
           active_item_class.push(value);
         }
       });
     });
     $("#rt-top .rt-fusionmenu .nopill").remove();
     $("#rt-top .rt-fusionmenu").append(menu_object1);
     $("#rt-top .rt-fusionmenu .nopill .menutop > li").removeClass("active");
     $.each(active_item_class, function(index, value){
       $("#rt-top .rt-fusionmenu .nopill .menutop ." + value).addClass("active");
     });
   } else if(menu_object1==null) {
     resizeMenu();
   }

   setSubMenu();

   $(window).resize(function() {
     resizeMenu();
     setSubMenu();
   });

   function resizeMenu() {
        /*
        500%	18px	
        400%	18px
        300%	18px>
        250%	18px>
        200%	18px
        175%	18px>
        150%	18px
        125%	18px>
        110%	18px>
        100%	17px
        90%	18px>
        75%	16px>
        67%	19px
        50%	18px>
        33%	2px>
        */

     $(".menutop li.root > .item").css("cssText", "padding: 0 16px !important");
        /* ---------- SET RIGHT AND LEFT PADDING ---------- */
        setTimeout(function() {
            var mn                      = $("ul.menutop.level1 li.root.lastItem > a")[0].getBoundingClientRect(); /* coords of element a in lastItem */
            var art                     = $("#rt-main .rt-containerInner")[0].getBoundingClientRect(); /* coords of rt-main */
            var pad_left                = Math.round($(".menutop li.root > .item").css("padding-left").replace("px", "")); /* padding of menu's elements */

            var all_el_width            = 0; /* width of all menu's elements */
            var el_right                = 0; /* x-coord of element's right side */
            var el_count                = 8; /* count of menu's elements which has padding that can modify */

            $($("ul.menutop.level1 > li.root").get().reverse()).each(function() {
                all_el_width += $(this).width();
                if (all_el_width < Math.abs($(this).find("a")[0].getBoundingClientRect().right - art.right)) {
                    el_right = $(this).find("a")[0].getBoundingClientRect().right;
                } else {
                    all_el_width -= $(this).width();
                    el_right = $(this).find("a")[0].getBoundingClientRect().right;
                    return false;
                }
            });

            /* calculate value of padding */
            padding_value = (-1) * (((el_right + all_el_width) - art.right) / (el_count*2));

            if (padding_value == 1) {
                padding_value = 0; /* for Mozilla Firefox */
            }

            int_padding_value0 = Math.abs(parseInt(padding_value)) + 0.9;
            int_padding_value1 = Math.abs(parseInt(padding_value)) + 1;
            diff_int_padding_value = Math.abs(padding_value) - Math.abs(parseInt(padding_value));

            if ((Math.abs(padding_value) >= int_padding_value0) && (Math.abs(padding_value) < int_padding_value1)) {
                if (padding_value > 0) {
                    padding_value++;
                } else if (padding_value < 0) {
                    //padding_value++;
                }
            }

            if (padding_value < 0) {
                padding_value = Math.round(padding_value);
            }

            padding_value = parseInt(padding_value);
            padding_value += pad_left;

            $(".menutop li.root > .item").css("cssText", "padding: 0 " + padding_value + "px !important");
        }, 500);
        /* ---------- SET RIGHT AND LEFT PADDING ---------- */

        /* ---------- SET MENU'S ELEMENTS WIDTH ---------- */
        setTimeout(function() {
            var mn       = $("ul.menutop.level1 li.root.lastItem > a")[0].getBoundingClientRect();
            var art      = $("#rt-main .rt-containerInner")[0].getBoundingClientRect();
            var pad_left = Math.round($(".menutop li.root > .item").css("padding-left").replace("px", ""));

            if (Math.abs(mn.right - art.right) >= 2 && Math.abs(mn.right - art.right) <= 100) {
                w_diff = Math.round(Math.abs(mn.right - art.right));
                w_diff_step = Math.round(w_diff / 20);

		if (w_diff_step == 0) {
                    w_diff_step = 1;
                }

                $(".menutop li.root:not(.firstItem):not(.lastItem) > .item").each(function() {
                    $(this).width($(this).width() + w_diff_step);
                    w_diff = w_diff - w_diff_step;
                    if (w_diff <= 3) {
                        return false;
                    }
                });
            }
            /* LOCAL STORAGE */
            if(art.top < 300) {
              menu_object = $("#rt-top .rt-fusionmenu").clone(true);
              localStorage.setItem('menu_source', JSON.stringify(menu_object.html()));
              localStorage.setItem('menu_zoom_index', (window.screen.availWidth/$("#rt-main .rt-container")[0].getBoundingClientRect().right).toFixed(2));
              localStorage.setItem('marker_width', ($("#rt-main .rt-container")[0].getBoundingClientRect().right - $("#rt-main .rt-container")[0].getBoundingClientRect().left).toFixed(2));
            }
            /*LOCAL STORAGE*/
       }, 1100);
       /* ---------- SET MENU'S ELEMENTS WIDTH ---------- */
    }    

    function setSubMenu() {
      setTimeout(function() {
        var pad_left = Math.round($(".menutop li.root > .item").css("padding-left").replace("px", ""));
        $(".menutop ul.level2").each(function() {
          $(this).css("cssText", "width:" + ($(this).parent().parent().width() - 10) + "px !important");
          $(this).find("span").css("cssText", "width:" + ($(this).parent().parent().width() - 22) + "px !important");
        });
      },1150);
    }
});